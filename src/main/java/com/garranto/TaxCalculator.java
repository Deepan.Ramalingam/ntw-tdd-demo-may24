package com.garranto;

public class TaxCalculator {

    BasicMathOperations basicMathOperations;

    public TaxCalculator(BasicMathOperations basicMathOperations){
        this.basicMathOperations = basicMathOperations;
    }
    public TaxCalculator(){}

    public double computeTax(int income, double roi){

        System.out.println("Compute tax in invoked");
        if(income > 0){
            return basicMathOperations.multiply(income,roi);
        }
       throw new IllegalArgumentException();
    }
}
