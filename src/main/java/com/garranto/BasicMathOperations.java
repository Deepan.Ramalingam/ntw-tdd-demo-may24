package com.garranto;

public class BasicMathOperations {

    public int multiplyInt(int integerOne, int integerTwo){
        return integerOne*integerTwo;
    }

    public double multiply(int inputOne, double inputTwo ){
        System.out.println("multiply method invoked");
        return inputOne * inputTwo;
    }
}
