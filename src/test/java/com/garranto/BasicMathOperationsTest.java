package com.garranto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BasicMathOperationsTest {

    @Test
    public void isBasicMathOperationsInstantiatable() {

        BasicMathOperations basicMathOperations = new BasicMathOperations();
        Assertions.assertNotNull(basicMathOperations);
    }

    @Test
    public void givenValidInputsMultiplyShouldReturnValidResults() {

        BasicMathOperations basicMathOperations = new BasicMathOperations();
        int inputOne = 2;
        int inputTwo = 3;
        int expected = 6;
        int actual = basicMathOperations.multiplyInt(inputOne, inputTwo);
        Assertions.assertEquals(expected, actual);
    }
}
