package com.garranto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TaxCalculatorTest {
    @Test
    public void isTaxCalculatorInstantiatable(){

        Assertions.assertNotNull(new TaxCalculator());
    }

    @Test
    public void givenValidInputsComputeTaxShouldReturnValidTaxableAmount(){

        TaxCalculator taxCalculator = new TaxCalculator(new BasicMathOperations());
        int income = 1000;
        double roi = 0.2;
        double expected = 200.0;
        double actual = taxCalculator.computeTax(income,roi);

        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void givenNegativeIncomeComputeTaxShouldThrowInvalidArgumentsException(){
        TaxCalculator taxCalculator = new TaxCalculator(new BasicMathOperations());
        int income = -1000;
        double roi = 0.2;



        Assertions.assertThrows(IllegalArgumentException.class,()->{
            taxCalculator.computeTax(income,roi);
        });
    }


}
